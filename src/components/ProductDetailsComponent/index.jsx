import React from "react";

import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';

const ProductDetailComponent = props => {
    const useStyles = makeStyles((theme) => ({
        root: {
            flexGrow: 1,
        },
        paper: {
            padding: theme.spacing(2),
            textAlign: 'center',
            color: theme.palette.text.secondary,
        },

    }));
    return (
        <div className={useStyles().root}>

            <Grid container spacing={3}>
                <Grid item xs={12}>
                    <h1>Product Details Page</h1>
                </Grid>
            </Grid>
            <Grid container spacing={3}>
                {
                    Object.keys(props.selectedProductData).length > 0 ?
                        <Grid item xs={12} >
                            <Paper className={useStyles.paper}>
                                Name : {props.selectedProductData.name}
                            </Paper>

                            <Paper className={useStyles.paper}>Price: {props.selectedProductData.price}</Paper>
                            <Paper className={useStyles.paper}>Description: {props.selectedProductData.description}</Paper>
                            <br />
                        </Grid>
                        :
                        <Grid item xs={12}>
                            <h3>No Data Found</h3>
                        </Grid>
                }
                <Grid item xs={12}>
                    <Button variant="contained" color="primary" onClick={(e) => {
                        props.redirectToPreviousPage(e)
                    }}>
                        Back
      </Button>
                </Grid>

            </Grid>
        </div>
    );

};

export default ProductDetailComponent;