import React from "react";

import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';

import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';


import GridViewComponent from "../GridViewComponent";

const ProductListComponent = props => {
    const useStyles = makeStyles((theme) => ({
        root: {
            flexGrow: 1,
        },
        
        formControl: {
            margin: theme.spacing(1),
            minWidth: 120,
        },
        selectEmpty: {
            marginTop: theme.spacing(2),
        }
    }));

    return (
        <div className={useStyles().root}>
            <Grid container spacing={3}>
                <Grid item xs={12}>
                    <h1>Product List Page</h1>
                </Grid>
                <Grid item xs={6}>
                    <h3>Product Categories</h3>
                </Grid>
                <Grid item xs={6}>
                    <FormControl className={useStyles().formControl}>
                        <Select
                            value={props.productListStates.selectedCategory}
                            onChange={(e) => { props.handleCategoryChange(e) }}
                            displayEmpty
                            className={useStyles().selectEmpty}
                            inputProps={{ 'aria-label': 'Without label' }}
                        >
                            <MenuItem value=""> Select Product Categories </MenuItem>
                            {Array.isArray(props.productCategories) && props.productCategories.length > 0
                                && props.productCategories.map((prodCategoryData, index) => {
                                    return (
                                        <MenuItem value={prodCategoryData.id} key={index}>{prodCategoryData.name}</MenuItem>
                                    );
                                })

                            }
                        </Select>
                    </FormControl>
                </Grid>
            </Grid>

            <Grid container spacing={3}>
                {
                    Array.isArray(props.productListData) && props.productListData.length > 0 ?
                        (props.productListStates.shouldShowCategoryData ?
                            props.productListStates.categoryData.map((categoryData, index) => {
                                return (
                                    <GridViewComponent
                                    handleProductTitleClick = {props.handleProductTitleClick}
                                    productData={categoryData}
                                    />
                                );
                            })
                            :
                            props.productListData.map((prodData, index) => {
                                return (
                                    <GridViewComponent
                                    handleProductTitleClick = {props.handleProductTitleClick}
                                    productData={prodData}
                                    />
                                );
                            })
                        ) :
                        <Grid item xs={12}>
                            <h3>No Data Found</h3>
                        </Grid>

                }
            </Grid>


        </div>
    );

}

export default ProductListComponent;