import React from "react";

const ResourceNotFound = props => {
  return (
    <>
      <h2>Resource Not found...</h2>
    </>
  );
};

export default ResourceNotFound;
