import React, { useState } from "react";
import "./style.css";

const Login = ({
  onSubmit,
  emailValidationError,
  passwordValidationError,
  invalidCredentialsError
}) => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  return (
    <div className="container text-center login-form">
      <form
        onSubmit={event => {
          event.preventDefault();
          onSubmit(email, password, event);
        }}
      >
        <div className="form-group">
          <label for="exampleInputEmail1">Email address</label>
          <input
            type="email"
            value={email}
            className="form-control"
            name="email"
            aria-describedby="emailHelp"
            placeholder="Enter email"
            onChange={e => {
              e.preventDefault();
              setEmail(e.target.value);
            }}
          />

          <small
            id="emailHelp"
            className={`form-text${emailValidationError ? " error" : ""}`}
          >
            {!emailValidationError
              ? "We'll never share your email with anyone else."
              : emailValidationError}
          </small>
        </div>
        <div className="form-group">
          <label for="exampleInputPassword1">Password</label>
          <input
            type="password"
            value={password}
            onChange={e => {
              e.preventDefault();
              setPassword(e.target.value);
            }}
            className="form-control"
            name="password"
            placeholder="Password"
          />
          {passwordValidationError && (
            <small className="form-text error">{passwordValidationError}</small>
          )}
        </div>
        <button type="submit" className="btn btn-primary">
          Login
        </button>
        {invalidCredentialsError && (
          <small className="form-text error">{invalidCredentialsError}</small>
        )}
      </form>
    </div>
  );
};

export default Login;
