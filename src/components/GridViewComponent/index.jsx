import React from "react";

import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import Link from '@material-ui/core/Link';

const ViewGridDataComponent = props => {
    const useStyles = makeStyles((theme) => ({
        paper: {
            padding: theme.spacing(2),
            textAlign: 'center',
            color: theme.palette.text.secondary,
        },

    }));
    return (
        <>
            <Grid item xs={12} sm={6}>
                <Paper className={useStyles.paper}>
                    Name :
                                            <Link component="button"
                        onClick={(event) => {
                            props.handleProductTitleClick(event, props.productData.id)
                        }}>
                        {props.productData.name}
                        </Link>
                </Paper>

                <Paper className={useStyles.paper}>Model: {props.productData.model}</Paper>
                <Paper className={useStyles.paper}>Price: {props.productData.price}</Paper>
                <br />
            </Grid>
        </>

    );

};

export default ViewGridDataComponent;