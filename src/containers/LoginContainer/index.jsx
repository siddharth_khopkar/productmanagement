import React,{useEffect,useState} from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import Login from "../../components/Login";
import history from "../../history";
import * as loginAction from "./actions";
import { getValue } from "../../utilities/app";

const LoginContainer = props => {
  function onLogin() {
    history.push("/resourcenotfound");
  }

  return (<Login onSubmit={onLogin} />);
};

const mapStateToProps = state => {
  return {
    // registration: state.authenticationReducer.registration
  };
};

const mapDispatchToProps = dispatch => {
  return {
    loginAction: bindActionCreators(loginAction, dispatch)
  };
};


export default connect(mapStateToProps,mapDispatchToProps)(LoginContainer);
