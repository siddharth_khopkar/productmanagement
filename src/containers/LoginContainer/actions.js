import apiService from "../../services/apiService";
import {ACTION_TYPE} from "../../redux/actionTypes";
import { getValue } from "../../utilities/app";


export function setUserRegistered(user) {
    return {
      type: ACTION_TYPE,
      data: user
    };
  }
  
  export function registerUser(data) {
    return dispatch => {
      apiService.userRegistration(data).then(
        response => {
          const user = getValue(response, "response.data");
          dispatch(setUserRegistered(user));
        },
        error => {
          const status = getValue(error, "error.response.status");
  
          if (status === 403) {
            dispatch(setUserRegistered({ userExist: true }));
            return;
          } else if (status === 400) {
            dispatch(setUserRegistered({ invalidEmail: true }));
          } else {
            dispatch(setUserRegistered({ serverError: true }));
            return;
          }
        }
      );
    };
  }