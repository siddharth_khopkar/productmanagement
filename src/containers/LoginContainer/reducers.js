import {ACTION_TYPE} from "../../redux/actionTypes";

export default function login(
    state = {
      registration: {}
    },
    action
  ) {
    switch (action.type) {
      case ACTION_TYPE:
        return Object.assign([], state, {
          registration: action.registration
        });
      default:
        return state;
    }
  }