import apiService from "../../services/apiService";
import {GET_PRODUCTS_LISTS,GET_PRODUCT_CATEGORIES} from "../../redux/actionTypes";
import { getValue } from "../../utilities/app";
import { API_SUCESS_STATUS } from "../../App.config";


export function setProductListData(productData) {
    return {
      type: GET_PRODUCTS_LISTS,
      productData
    };
  }
  
  export function getProductList() {
   
    return dispatch => {
      apiService.getProductList().then(
        response => {
             if(response.status == API_SUCESS_STATUS){
                 dispatch(setProductListData(response.data));
             }
        },
        error => {
       
        }
      );
    };
  }

  export function setProductCategoryData(productCategories) {
    return {
      type: GET_PRODUCT_CATEGORIES,
      productCategories
    };
  }

  export function getCategoryList() {
    return dispatch => {
      // dispatch(setProductCategoryData({}));
      apiService.getCategoryList().then(
        response => {
            if(response.status == API_SUCESS_STATUS){
                 dispatch(setProductCategoryData(response.data));
             }
        },
        error => {
       
        }
      );
    };
  }