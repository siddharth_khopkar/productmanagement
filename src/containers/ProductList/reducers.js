

  import {GET_PRODUCTS_LISTS,GET_PRODUCT_CATEGORIES} from "../../redux/actionTypes";

export default function ProductList(
    state = {
        productData: [],
        productCategories: []
    },
    action
  ) {
    switch (action.type) {
      case GET_PRODUCTS_LISTS:
        return Object.assign({}, state, {
            productData: action.productData
        });
        case GET_PRODUCT_CATEGORIES:
            return Object.assign({}, state, {
                productCategories: action.productCategories
            });
      default:
        return state;
    }
  }