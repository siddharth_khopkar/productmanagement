import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import Login from "../../components/Login";
import history from "../../history";
import { getValue } from "../../utilities/app";
import ProductListComponent from "../../components/ProductListComponent";
import * as ProductListAction from "./actions";
import * as ProductDetailAction from "../ProductDetail/actions";

class ProductListContainer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedCategory: "",
            shouldShowCategoryData: false,
            categoryData: []
        };
        this.handleCategoryChange = this.handleCategoryChange.bind(this);
        this.getCategoryData = this.getCategoryData.bind(this);
        this.handleProductTitleClick =   this.handleProductTitleClick.bind(this);
     

    }

    componentDidMount() {
        this.props.ProductListAction.getProductList();
        this.props.ProductListAction.getCategoryList();
    }
    componentWillUnmount(){
        this.props.ProductListAction.setProductListData([]);
        this.props.ProductListAction.setProductCategoryData([]);
    }

    componentDidUpdate(prevProps,prevState){
        if(this.props.selectedProductData != prevProps.selectedProductData) {
            this.redirectToNextPage();
        }
    }


    handleCategoryChange(event) {
        this.setState({ selectedCategory: event.target.value }, () => {
            this.getCategoryData(event.target.value);
        });


    }

    getCategoryData(categoryValue) {
        let filteredCatalogData = [];
        let shouldShowCategoryDataFlag = false;
        if (typeof categoryValue == "number") {
            Array.isArray(this.props.productListData) && this.props.productListData.length > 0 &&
                this.props.productListData.forEach((data) => {
                  
                    if (data.categoryId == categoryValue) {
                        if (filteredCatalogData && filteredCatalogData.length > 0) {
                            let previousArrayIndex = filteredCatalogData.length - 1;
                            filteredCatalogData[previousArrayIndex + 1] = data;
                        }
                        else {
                            filteredCatalogData.push(data);
                        }
                        shouldShowCategoryDataFlag = true;
                    }
                })
        }

        this.setState({categoryData: filteredCatalogData,shouldShowCategoryData: shouldShowCategoryDataFlag});

    }

    handleProductTitleClick(event,productId){
         event.preventDefault();
        this.props.ProductDetailAction.getProductDetils(productId);
       this.redirectToNextPage();
    }

    redirectToNextPage(){
        history.push("/ProductDetails");
    }


    render() {
        return (
            <>
                <ProductListComponent
                    productListData={this.props.productListData}
                    productCategories={this.props.productCategories}
                    productListStates={this.state}
                    handleCategoryChange={this.handleCategoryChange}
                    handleProductTitleClick={this.handleProductTitleClick}
                />
            </>
        );
    }
}

const mapStateToProps = state => {
    return {
        productListData: state.productList.productData,
        productCategories: state.productList.productCategories,
        selectedProductData: state.productDetails.selectedProductData
    };
};

const mapDispatchToProps = dispatch => {
    return {
        ProductListAction: bindActionCreators(ProductListAction, dispatch),
        ProductDetailAction: bindActionCreators(ProductDetailAction, dispatch)
    };
};


export default connect(mapStateToProps, mapDispatchToProps)(ProductListContainer);
