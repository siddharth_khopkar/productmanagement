

  import {GET_SELECTED_PRODUCT_DATA} from "../../redux/actionTypes";

  export default function ProductDetail(
      state = {
          selectedProductData: {}
      },
      action
    ) {
      switch (action.type) {
        case GET_SELECTED_PRODUCT_DATA:
          return Object.assign({}, state, {
            selectedProductData: action.selectedProductData
          });
         
        default:
          return state;
      }
    }