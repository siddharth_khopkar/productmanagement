import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import Login from "../../components/Login";
import history from "../../history";
import * as ProductDetailsAction from "./actions";
import ProductDetilsComponent from "../../components/ProductDetailsComponent";


class ProductDetailsContainer extends Component {
    constructor(props) {
        super(props);
        this.state = {

        };
        this.redirectToPreviousPage = this.redirectToPreviousPage.bind(this);

    }

    componentDidMount() {
        if (Object.keys(this.props.selectedProductData).length == 0) {
            history.push("/");
        }
    }

    componentWillUnmount() {
        // this.props.ProductDetailsAction.setSelectedProductData({});
    }
    redirectToPreviousPage(e) {
        e.preventDefault();
        window.history.back();
    }

    render() {
        return (
            <>
                <ProductDetilsComponent
                    selectedProductData={this.props.selectedProductData}
                    redirectToPreviousPage={this.redirectToPreviousPage}
                />
            </>
        );
    }
}

const mapStateToProps = state => {
    return {
        selectedProductData: state.productDetails.selectedProductData
    };
};

const mapDispatchToProps = dispatch => {
    return {
        ProductDetailsAction: bindActionCreators(ProductDetailsAction, dispatch)
    };
};


export default connect(mapStateToProps, mapDispatchToProps)(ProductDetailsContainer);