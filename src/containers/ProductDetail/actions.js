import apiService from "../../services/apiService";
import {GET_SELECTED_PRODUCT_DATA} from "../../redux/actionTypes";
import { getValue } from "../../utilities/app";
import { API_SUCESS_STATUS } from "../../App.config";

export function setSelectedProductData(data){
  return{
    type: GET_SELECTED_PRODUCT_DATA,
    selectedProductData: data
  };
}

export function getProductDetils(productId) {
    return dispatch => {
      dispatch(setSelectedProductData({}));
      apiService.getProductDetails(productId).then(
        response => {
             if(response.status == API_SUCESS_STATUS){
                  dispatch(setSelectedProductData(response.data));
             }
        },
        error => {
       
        }
      );
    };
  }