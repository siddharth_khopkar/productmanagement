import history from "../history";
import _ from "lodash";




export const EMAIL_PATTERN =
  "^[A-Za-z0-9._%-+]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}$";
export const PASSWORD_PATTERN =
  "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[!\"#$%&'()*+,-./:;<=>?@[\\]^_`{|}~])[A-Za-z\\d!\"#$%&'()*+,-./:;<=>?@[\\]^_`{|}~]{8,20}$";
export const ALPHABET_PATTERN_WITH_SPECIAL_CHARACTERS = "^[a-zA-Z '-]*$";
export const NUMBER_PATTERN = "^[0-9]*$";
export const COUNTRY_CODE_PATTERN = "[\\+][0-9]{1,5}";



export const getValue = (obj, expression) => {
  try {
    return expression.split(".").reduce((o, x, i) => {
      if (i === 0) {
        return o;
      }
      return typeof o === "undefined" || o === null ? undefined : o[x];
    }, obj);
  } catch (e) {
    console.error("getValue => " + e);
    return undefined;
  }
};



