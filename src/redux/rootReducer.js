import { combineReducers } from "redux";
import loginReducer from "../containers/LoginContainer/reducers";
import productListReducer from "../containers/ProductList/reducers";
import productDetailsReducer from "../containers/ProductDetail/reducers";

export default combineReducers({
    login: loginReducer,
    productList: productListReducer,
    productDetails: productDetailsReducer
});
