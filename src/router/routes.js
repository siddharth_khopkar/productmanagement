import LoginContainer from "../containers/LoginContainer";
import AccessDenied from "../components/AccessDenied";
import ProductListContainer from "../containers/ProductList";
import ProductDetailsContainer from "../containers/ProductDetail";
import ResourceNotFound from "../components/ResourceNotFound";

const routes = [
  { path: ["/","/ProductList"], exact: true, component:ProductListContainer},
  { path: "/ProductDetails", exact: true, component: ProductDetailsContainer },
  { path: "/login", exact: true, component: LoginContainer },
  { path: "/someProtectedRoute", isProtected: true, component: AccessDenied },
  { path: "/resourcenotfound", component: ResourceNotFound },
  { component: ResourceNotFound }
];

export default routes;
